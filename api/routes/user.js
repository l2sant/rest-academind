const express = require('express')
const router = express.Router()

const usersCtrl = require('../controllers/users')
const checkAuth = require('../middleware/check-auth')


router.post('/signup', usersCtrl.signUp)

router.post('/login', usersCtrl.login)

router.delete('/:userId', checkAuth, usersCtrl.delOneUser)


module.exports = router