const mongoose =  require('mongoose')
const Product = require('../models/product')




module.exports.createProduct = (req, res, next) => {
    console.log(req.file)
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path

    })

    product.save().then(result => {
            res.status(201).json({
                msg: 'Created product successfully',
                createdProduct: {
                    name: result.name,
                    price: result.price,
                    _id: result._id
                },
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/product/' + result._id

                }
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
}


module.exports.getAllProducts = (req, res, next) => {
    Product.find()
        .select('name price _id productImage')
        .then(docs => {
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        productImage: doc.productImage,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/product/' + doc._id
                        }
                    }
                })
            }

            res.status(200).json(response)

        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}

module.exports.getOneProduct = (req, res, next) => {
    const productId = req.params.productId
    Product.findById(productId)
        .select('name price _id productImage')
        .then(doc => {
            if (doc) {
                res.status(200).json({
                    product: doc,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/product/'
                    }
                })
            } else {
                res.status(404).json({
                    msg: 'no valid entry found for provided ID'
                })
            }
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}



module.exports.delOneProduct = (req, res, next) => {
    const id = req.params.id
    Product.deleteOne({
            _id: id
        })
        .then(result => {
            res.status(200).json({
                msg: 'Product deleted',
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/product/',
                    data: {
                        name: 'String',
                        price: 'Number'
                    }
                }
            })

        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}


module.exports.updateOneProduct = (req, res, next) => {
    const id = req.params.productId
    const updateOps = {}
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }

    Product.update({ _id: id}, { $set: updateOps})
        .then(result => {
            res.status(200).json({
                msg: 'Product updated',
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/product/' + id
                }
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            })
        })
}