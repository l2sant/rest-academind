const express =  require('express')
const app =  express()
const mongoose = require('mongoose')

const mongo = process.env.MONGO || "mongodb://localhost:27017/rest-academind"

mongoose.set('useCreateIndex', true);
mongoose.connect(mongo, { useNewUrlParser: true })

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
    if(req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE')
        return res.status(200).json({})
    }
    next()
})


const productsRoutes = require('./api/routes/products')
const ordersRouters = require('./api/routes/order')
const userRouters = require('./api/routes/user')

app.use('/uploads', express.static('uploads'))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use('/product', productsRoutes)
app.use('/orders', ordersRouters)
app.use('/user', userRouters)

app.use((req, res, next) => {
    const error = new Error('not found')
    error.status(404)
    next(error)
})

app.use((error, req, res) => {
    res.status(err.status || 500)
    res.json({
        error: {
            msg:error.message
        }
    })
})

module.exports = app